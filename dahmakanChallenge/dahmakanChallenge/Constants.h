//
//  Constants.h
//  dahmakanChallenge
//
//  Created by Fahim Surani on 4/28/18.
//  Copyright © 2018 Example. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constants : NSObject

#pragma mark - Enpoints
extern NSString * const URL_ENDPOINT_ORDERS;

@end
