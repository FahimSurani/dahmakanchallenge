//
//  Constants.m
//  dahmakanChallenge
//
//  Created by Fahim Surani on 4/28/18.
//  Copyright © 2018 Example. All rights reserved.
//

#import "Constants.h"

@implementation Constants

NSString * const URL_ENDPOINT_ORDERS        = @"http://staging-api.dahmakan.com/test/orders";

@end
