//
//  OrdersEngine.h
//  dahmakanChallenge
//
//  Created by Fahim Surani on 4/28/18.
//  Copyright © 2018 Example. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol OrdersEngine_Delegate <NSObject>

- (void)ordersEngineCompletedWithObject:(NSArray *)object;
- (void)ordersEngineFailedWithError:(NSError*)error;

@end


@interface OrdersEngine : NSObject

@property (nonatomic, weak) id<OrdersEngine_Delegate> delegate;


- (id)initWithDelegate:(id)delegate_;
- (void)getOrdersApi;


@end
