//
//  OrdersEngine.m
//  dahmakanChallenge
//
//  Created by Fahim Surani on 4/28/18.
//  Copyright © 2018 Example. All rights reserved.
//

#import "OrdersEngine.h"
#import "Constants.h"
#import "OrderResponse.h"
#import "Order.h"


@implementation OrdersEngine

-(id)initWithDelegate:(id)delegate_ {
    
    self = [super init];
    if (self) {
        self.delegate = delegate_;
    }
    return self;
}

-(void)getOrdersApi{
    NSURL *url = [[NSURL alloc]initWithString:URL_ENDPOINT_ORDERS];
    NSURLSession *session = [NSURLSession sharedSession];  //use NSURLSession class
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url];
    [request setHTTPMethod:@"GET"];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data && !error) {
            NSDictionary *object =  (NSDictionary *)[NSJSONSerialization
                         JSONObjectWithData:data
                         options:0
                         error:nil];
            
            OrderResponse *orderResponse = [[OrderResponse alloc] initWithDictionary:object];
            if (self.delegate && [self.delegate conformsToProtocol:@protocol(OrdersEngine_Delegate)]) {
                [self.delegate ordersEngineCompletedWithObject:[self sortByUTC:orderResponse.orders]];
            }
            
        }else{
            if (self.delegate && [self.delegate conformsToProtocol:@protocol(OrdersEngine_Delegate)]) {
                [self.delegate ordersEngineFailedWithError:error];
            }
        }
    }];
    [task resume]; // to start the download task
}

- (NSArray *)sortByUTC:(NSArray *)arrayToSort{
    if (!arrayToSort) {
        return nil;
    }
    return [arrayToSort sortedArrayUsingComparator:^NSComparisonResult(Order *obj1, Order *obj2) {
        if (obj1.arrivesAtUtc > obj2.arrivesAtUtc) {
            return (NSComparisonResult)NSOrderedDescending;
        } else {
            return (NSComparisonResult)NSOrderedAscending;
        }
    }];
}

@end
