//
//	Order.h
//
//	Create by Fahim Surani on 28/4/2018
//	Copyright © 2018. All rights reserved.
//

//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

#import <UIKit/UIKit.h>

@interface Order : NSObject

@property (nonatomic, strong) NSDate *arrivesAtUtc;
@property (nonatomic, assign) NSInteger orderId;
@property (nonatomic, strong) NSString * paidWith;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end
