//
//	Order.m
//
//	Create by Fahim Surani on 28/4/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport



#import "Order.h"

NSString *const kOrderArrivesAtUtc = @"arrives_at_utc";
NSString *const kOrderOrderId = @"order_id";
NSString *const kOrderPaidWith = @"paid_with";

@interface Order ()
@end
@implementation Order




/**
 * Instantiate the instance using the passed dictionary values to set the properties values
 */

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if(![dictionary[kOrderArrivesAtUtc] isKindOfClass:[NSNull class]]){
        self.arrivesAtUtc = [NSDate dateWithTimeIntervalSince1970:[dictionary[kOrderArrivesAtUtc] doubleValue]];
	}

	if(![dictionary[kOrderOrderId] isKindOfClass:[NSNull class]]){
		self.orderId = [dictionary[kOrderOrderId] integerValue];
	}

	if(![dictionary[kOrderPaidWith] isKindOfClass:[NSNull class]]){
		self.paidWith = dictionary[kOrderPaidWith];
	}	
	return self;
}


/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
-(NSDictionary *)toDictionary
{
	NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
	dictionary[kOrderArrivesAtUtc] = self.arrivesAtUtc;
	dictionary[kOrderOrderId] = @(self.orderId);
	if(self.paidWith != nil){
		dictionary[kOrderPaidWith] = self.paidWith;
	}
	return dictionary;

}

/**
 * Implementation of NSCoding encoding method
 */
/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
- (void)encodeWithCoder:(NSCoder *)aCoder
{
    if (self.arrivesAtUtc != nil) {
        [aCoder encodeObject:self.arrivesAtUtc forKey:kOrderArrivesAtUtc];
    }
    [aCoder encodeObject:@(self.orderId) forKey:kOrderOrderId];
    if(self.paidWith != nil){
		[aCoder encodeObject:self.paidWith forKey:kOrderPaidWith];
	}

}

/**
 * Implementation of NSCoding initWithCoder: method
 */
- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
	self = [super init];
	self.arrivesAtUtc = [aDecoder decodeObjectForKey:kOrderArrivesAtUtc];
	self.orderId = [[aDecoder decodeObjectForKey:kOrderOrderId] integerValue];
	self.paidWith = [aDecoder decodeObjectForKey:kOrderPaidWith];
	return self;

}

/**
 * Implementation of NSCopying copyWithZone: method
 */
- (instancetype)copyWithZone:(NSZone *)zone
{
	Order *copy = [Order new];

	copy.arrivesAtUtc = self.arrivesAtUtc;
	copy.orderId = self.orderId;
	copy.paidWith = [self.paidWith copy];

	return copy;
}
@end
