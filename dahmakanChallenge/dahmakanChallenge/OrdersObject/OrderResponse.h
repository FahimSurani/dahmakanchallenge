//
//	OrderResponse.h
//
//	Create by Fahim Surani on 28/4/2018
//	Copyright © 2018. All rights reserved.
//

//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

#import <UIKit/UIKit.h>
#import "Order.h"

@interface OrderResponse : NSObject

@property (nonatomic, strong) NSArray * orders;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end