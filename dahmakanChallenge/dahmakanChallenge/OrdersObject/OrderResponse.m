//
//	OrderResponse.m
//
//	Create by Fahim Surani on 28/4/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport



#import "OrderResponse.h"

NSString *const kOrderResponseOrders = @"orders";

@interface OrderResponse ()
@end
@implementation OrderResponse




/**
 * Instantiate the instance using the passed dictionary values to set the properties values
 */

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if(dictionary[kOrderResponseOrders] != nil && [dictionary[kOrderResponseOrders] isKindOfClass:[NSArray class]]){
		NSArray * ordersDictionaries = dictionary[kOrderResponseOrders];
		NSMutableArray * ordersItems = [NSMutableArray array];
		for(NSDictionary * ordersDictionary in ordersDictionaries){
			Order * ordersItem = [[Order alloc] initWithDictionary:ordersDictionary];
			[ordersItems addObject:ordersItem];
		}
		self.orders = ordersItems;
	}
	return self;
}


/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
-(NSDictionary *)toDictionary
{
	NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
	if(self.orders != nil){
		NSMutableArray * dictionaryElements = [NSMutableArray array];
		for(Order * ordersElement in self.orders){
			[dictionaryElements addObject:[ordersElement toDictionary]];
		}
		dictionary[kOrderResponseOrders] = dictionaryElements;
	}
	return dictionary;

}

/**
 * Implementation of NSCoding encoding method
 */
/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
- (void)encodeWithCoder:(NSCoder *)aCoder
{
	if(self.orders != nil){
		[aCoder encodeObject:self.orders forKey:kOrderResponseOrders];
	}

}

/**
 * Implementation of NSCoding initWithCoder: method
 */
- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
	self = [super init];
	self.orders = [aDecoder decodeObjectForKey:kOrderResponseOrders];
	return self;

}

/**
 * Implementation of NSCopying copyWithZone: method
 */
- (instancetype)copyWithZone:(NSZone *)zone
{
	OrderResponse *copy = [OrderResponse new];

	copy.orders = [self.orders copy];

	return copy;
}
@end