//
//  ViewController.m
//  dahmakanChallenge
//
//  Created by Fahim Surani on 4/28/18.
//  Copyright © 2018 Example. All rights reserved.
//

#import "ViewController.h"
#import "OrdersEngine.h"
#import "OrderResponse.h"
#import "Order.h"

@interface ViewController ()<OrdersEngine_Delegate>{
    OrdersEngine *engine;
}
@end
@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    engine = [[OrdersEngine alloc] initWithDelegate:self];
    [engine getOrdersApi];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)ordersEngineCompletedWithObject:(NSArray *)object{
    //load data
    NSLog(@"%@", object);
    
}

-(void)ordersEngineFailedWithError:(NSError *)error{
    //Handle Fail State
}


@end
