//
//  main.m
//  dahmakanChallenge
//
//  Created by Fahim Surani on 4/28/18.
//  Copyright © 2018 Example. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
